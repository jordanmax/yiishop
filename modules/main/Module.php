<?php

namespace app\modules\main;
use Yii;

/**
 * main module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\main\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->registerTranslations();

        // custom initialization code goes here
    }
    public function registerTranslations()
    {
      if (!isset(Yii::$app->i18n->translations['category'])) {
        Yii::$app->i18n->translations['category'] = [
          'class' => 'yii\i18n\PhpMessageSource',
          'sourceLanguage' => 'en',
          'basePath' => '@app/modules/main/messages'
        ];
      }
    }
}
