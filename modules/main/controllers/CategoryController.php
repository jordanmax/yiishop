<?php

namespace app\modules\main\controllers;

use yii\web\Controller;
use app\modules\main\models\Category;
use app\modules\main\models\Product;
use Yii;

/**
 * Default controller for the `main` module
 */
class CategoryController extends AppController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
        return $this->render('index', compact('hits'));
    }
}
