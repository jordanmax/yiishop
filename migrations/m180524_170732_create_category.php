<?php

use yii\db\Migration;

/**
 * Class m180524_170732_create_category
 */
class m180524_170732_create_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable('category', [
            'id' => $this->primaryKey(),
            'parent_id' =>$this->integer(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180524_170732_create_category cannot be reverted.\n";

        return false;
    }
    */
}
