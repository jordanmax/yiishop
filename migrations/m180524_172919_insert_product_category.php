<?php

use yii\db\Migration;

/**
 * Class m180524_172919_insert_product_category
 */
class m180524_172919_insert_product_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->insert('category',
        [
          'parent_id' => '0',
          'name' => 'SPORTSWEAR',
        ]);
      $this->insert('category',
        [
          'parent_id' => '0',
          'name' => 'MENS',
        ]);
      $this->insert('category',
        [
          'parent_id' => '1',
          'name' => 'NIKE',
        ]);
      $this->insert('category',
        [
          'parent_id' => '1',
          'name' => 'UNDER ARMOUR',
        ]);
      $this->insert('product',
        [
              'category_id' => '1',
              'name' => 'Easy Polo Black Edition',
              'content' => 'Easy Polo Black EditionEasy Polo Black EditionEasy Polo Black EditionEasy Polo Black Edition',
              'price' => '100',
              'img' => '',
              'hit' => '1',
              'new' => '1',
              'sale' => '0',
        ]);
      $this->insert('product',
        [
              'category_id' => '2',
              'name' => 'Easy Polo Black Edition',
              'content' => 'Easy Polo Black EditionEasy Polo Black EditionEasy Polo Black EditionEasy Polo Black Edition',
              'price' => '100',
              'img' => '',
              'hit' => '1',
              'new' => '1',
              'sale' => '0',
        ]);
      $this->insert('product',
        [
              'category_id' => '3',
              'name' => 'Easy Polo Black Edition',
              'content' => 'Easy Polo Black EditionEasy Polo Black EditionEasy Polo Black EditionEasy Polo Black Edition',
              'price' => '100',
              'img' => '',
              'hit' => '1',
              'new' => '1',
              'sale' => '0',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
      $this->truncateTable('category');
      $this->truncateTable('product');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180524_172919_insert_product_category cannot be reverted.\n";

        return false;
    }
    */
}
