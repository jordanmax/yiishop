<?php

use yii\db\Migration;

/**
 * Class m180524_171242_create_product
 */
class m180524_171242_create_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $this->createTable('product', [
            'id' => $this->primaryKey(),
            'category_id' =>$this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'content' => $this->text()->notNull(),
            'price' => $this->float()->notNull(),
            'img' => $this->string(),
            'hit' => 'ENUM("0", "1")',
            'new' => 'ENUM("0", "1")',
            'sale' => 'ENUM("0", "1")',
        ]);
        $sql = "ALTER TABLE product ALTER hit SET DEFAULT '0'";
        $sql1 = "ALTER TABLE product ALTER new SET DEFAULT '0'";
        $sql2 = "ALTER TABLE product ALTER sale SET DEFAULT '0'";
        $sql3 = "ALTER TABLE product ALTER img SET DEFAULT 'no-image.png'";
        $this->execute($sql);
        $this->execute($sql1);
        $this->execute($sql2);
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180524_171242_create_product cannot be reverted.\n";

        return false;
    }
    */
}
